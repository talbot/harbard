-- Account number sequence
CREATE SEQUENCE account_number_seq INCREMENT 2 START WITH 1002;
COMMENT ON SEQUENCE account_number_seq IS 'Sequence for account number, always even for non-technical accounts';

-- Accounts table: stores account numbers and balance
CREATE TABLE account (
  id             BIGSERIAL PRIMARY KEY,
  account_number BIGINT                   NOT NULL DEFAULT nextval('account_number_seq') CHECK (account_number > 0),
  balance        NUMERIC(14, 2)           NOT NULL DEFAULT 0.00 CHECK (balance >= 0),
  created_at     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE UNIQUE INDEX idx_account_account_number ON account (account_number ASC);

COMMENT ON TABLE account IS 'Stores account numbers and balance';
COMMENT ON COLUMN account.id IS 'Synthetic key for account';
COMMENT ON COLUMN account.account_number IS 'Account number: should be even for non-technical accounts';
COMMENT ON COLUMN account.balance IS 'Account balance: could not be negative';
COMMENT ON COLUMN account.created_at IS 'Date and time of account creation';
COMMENT ON COLUMN account.updated_at IS 'Date and time of account update (mostly date of changes of balance)';
COMMENT ON INDEX idx_account_account_number IS 'Account numbers must be unique';

-- Transfers table: stores information about transfers between accounts
CREATE TABLE transfer (
  id                     BIGSERIAL PRIMARY KEY,
  source_account_id      BIGINT                   NOT NULL REFERENCES account (id),
  destination_account_id BIGINT                   NOT NULL REFERENCES account (id),
  amount                 NUMERIC(10, 2)           NOT NULL CHECK (amount > 0),
  created_at             TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE INDEX idx_transfer_source_destination ON transfer (source_account_id ASC, destination_account_id ASC);

COMMENT ON TABLE transfer IS 'Stores information about transfers between accounts';
COMMENT ON COLUMN transfer.id IS 'Synthetic key for transfer';
COMMENT ON COLUMN transfer.source_account_id IS 'Account ID from which the transfer was';
COMMENT ON COLUMN transfer.destination_account_id IS 'Account ID to which the transfer was';
COMMENT ON COLUMN transfer.amount IS 'Transfer amount, must be positive';
COMMENT ON COLUMN transfer.created_at IS 'Date and time of transfer creation';
COMMENT ON INDEX idx_transfer_source_destination IS 'Transfer between accounts index';

-- User profile table: stores information about user profile and link to account
CREATE TABLE user_profile (
  id         BIGSERIAL PRIMARY KEY,
  user_name  TEXT                     NOT NULL,
  account_id BIGINT                   NOT NULL REFERENCES account (id),
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE UNIQUE INDEX idx_user_profile_account_id ON user_profile (account_id ASC);

COMMENT ON TABLE user_profile IS 'Stores information about user profile and link to account';
COMMENT ON COLUMN user_profile.id IS 'Synthetic key for user profile';
COMMENT ON COLUMN user_profile.user_name IS 'The name of the user';
COMMENT ON COLUMN user_profile.account_id IS 'Account ID to which the profile linked';
COMMENT ON COLUMN user_profile.created_at IS 'Date and time of user profile creation';
COMMENT ON COLUMN user_profile.updated_at IS 'Date and time of user profile update';
COMMENT ON INDEX idx_user_profile_account_id IS 'Account numbers must be unique for users (one account per user)';

--
-- Add technical account: since money could not come from nowhere,
-- we must have technical account or technical gateways for that purpose.
-- Cash flow on this accounts is controlled by financial monitoring,
-- and every charge from this account must be reflected in transfer table
--
INSERT INTO account (id, account_number, balance) VALUES (-1, 1, 9999999990.99);
INSERT INTO user_profile (id, account_id, user_name) VALUES (-1, -1, 'Skrat: technical account')