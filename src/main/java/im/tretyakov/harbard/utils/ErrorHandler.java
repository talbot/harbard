package im.tretyakov.harbard.utils;

import im.tretyakov.harbard.process.ApiError;
import im.tretyakov.harbard.process.ApiErrorCode;
import im.tretyakov.harbard.process.ApiResult;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.UUID;

public final class ErrorHandler {

    public static final String INTERNAL_ERROR_MESSAGE = "Oops! Something went wrong here";

    public static ResponseEntity<Object> handleError(ApiErrorCode errorCode, String message, Throwable error, String errorId) {
        return new ResponseEntity<>(ApiResult.error(new ApiError(errorCode, message)), getHeaders(error, errorId),
                errorCode.getHttpStatus());
    }

    public static MultiValueMap<String, String> getHeaders(@Nullable Throwable throwable, @Nonnull String errorId) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Error-Id", errorId);
        if (throwable != null) {
            headers.add("Reason", throwable.getMessage());
        }
        return headers;
    }

    public static String getErrorId() {
        return UUID.randomUUID().toString();
    }
}
