package im.tretyakov.harbard.domain;

/**
 * User info DTO: user name and account number
 */
public class UserInfo {

    private final String userName;

    private final Long accountNumber;

    public UserInfo(String userName, Long accountNumber) {
        this.userName = userName;
        this.accountNumber = accountNumber;
    }

    public String getUserName() {
        return userName;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userName='" + userName + '\'' +
                ", accountNumber=" + accountNumber +
                '}';
    }
}
