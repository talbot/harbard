package im.tretyakov.harbard.domain;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class Transfer {

    private final Long id;

    private final Long sourceAccountId;

    private final Long destinationAccountId;

    private final BigDecimal amount;

    private final ZonedDateTime createdAt;

    private Transfer(Long id, Long sourceAccountId, Long destinationAccountId, BigDecimal amount, ZonedDateTime createdAt) {
        this.id = id;
        this.sourceAccountId = sourceAccountId;
        this.destinationAccountId = destinationAccountId;
        this.amount = amount;
        this.createdAt = createdAt;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public Long getSourceAccountId() {
        return sourceAccountId;
    }

    public Long getDestinationAccountId() {
        return destinationAccountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "id=" + id +
                ", sourceAccountId=" + sourceAccountId +
                ", destinationAccountId=" + destinationAccountId +
                ", amount=" + amount +
                ", createdAt=" + createdAt +
                '}';
    }

    public static final class Builder {
        private Long id;
        private Long sourceAccountId;
        private Long destinationAccountId;
        private BigDecimal amount;
        private ZonedDateTime createdAt;

        private Builder() {
        }

        @Nonnull
        public Builder withId(@Nonnull Long id) {
            this.id = id;
            return this;
        }

        @Nonnull
        public Builder withSourceAccountId(@Nonnull Long sourceAccountId) {
            this.sourceAccountId = sourceAccountId;
            return this;
        }

        @Nonnull
        public Builder withDestinationAccountId(@Nonnull Long destinationAccountId) {
            this.destinationAccountId = destinationAccountId;
            return this;
        }

        @Nonnull
        public Builder withAmount(@Nonnull BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        @Nonnull
        public Builder withCreatedAt(@Nonnull ZonedDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        @Nonnull
        public Transfer build() {
            return new Transfer(id, sourceAccountId, destinationAccountId, amount, createdAt);
        }
    }
}
