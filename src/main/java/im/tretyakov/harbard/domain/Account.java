package im.tretyakov.harbard.domain;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * Account domain
 */
public class Account {

    private final Long id;

    private final Long accountNumber;

    private final BigDecimal balance;

    private final ZonedDateTime createdAt;

    private final ZonedDateTime updatedAt;

    private Account(Long id, Long accountNumber, BigDecimal balance, ZonedDateTime createdAt, ZonedDateTime updatedAt) {
        this.id = id;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", accountNumber=" + accountNumber +
                ", balance=" + balance +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    public static final class Builder {
        private Long id;
        private Long accountNumber;
        private BigDecimal balance;
        private ZonedDateTime createdAt;
        private ZonedDateTime updatedAt;

        private Builder() {
        }

        @Nonnull
        public Builder withId(@Nonnull Long id) {
            this.id = id;
            return this;
        }

        @Nonnull
        public Builder withAccountNumber(@Nonnull Long accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        @Nonnull
        public Builder withBalance(@Nonnull BigDecimal balance) {
            this.balance = balance;
            return this;
        }

        @Nonnull
        public Builder withCreatedAt(@Nonnull ZonedDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        @Nonnull
        public Builder withUpdatedAt(@Nonnull ZonedDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        @Nonnull
        public Account build() {
            return new Account(id, accountNumber, balance, createdAt, updatedAt);
        }
    }
}
