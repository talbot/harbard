package im.tretyakov.harbard.domain;

import javax.annotation.Nonnull;
import java.time.ZonedDateTime;

public class UserProfile {

    private final Long id;

    private final Long accountId;

    private final String userName;

    private final ZonedDateTime createdAt;

    private final ZonedDateTime updatedAt;

    private UserProfile(Long id, Long accountId, String userName, ZonedDateTime createdAt, ZonedDateTime updatedAt) {
        this.id = id;
        this.accountId = accountId;
        this.userName = userName;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public String getUserName() {
        return userName;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", userName='" + userName + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    public static final class Builder {
        private Long id;
        private Long accountId;
        private String userName;
        private ZonedDateTime createdAt;
        private ZonedDateTime updatedAt;

        private Builder() {
        }

        @Nonnull
        public Builder withId(@Nonnull Long id) {
            this.id = id;
            return this;
        }

        @Nonnull
        public Builder withAccountId(@Nonnull Long accountId) {
            this.accountId = accountId;
            return this;
        }

        @Nonnull
        public Builder withUserName(@Nonnull String userName) {
            this.userName = userName;
            return this;
        }

        @Nonnull
        public Builder withCreatedAt(@Nonnull ZonedDateTime createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        @Nonnull
        public Builder withUpdatedAt(@Nonnull ZonedDateTime updatedAt) {
            this.updatedAt = updatedAt;
            return this;
        }

        @Nonnull
        public UserProfile build() {
            return new UserProfile(id, accountId, userName, createdAt, updatedAt);
        }
    }
}
