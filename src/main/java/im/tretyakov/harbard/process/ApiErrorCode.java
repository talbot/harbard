package im.tretyakov.harbard.process;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import org.springframework.http.HttpStatus;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Enumeration of API error codes
 */
@ApiModel("Unique code for error")
public enum ApiErrorCode {
    ACCOUNT_NOT_FOUND(HttpStatus.NOT_FOUND, "account-not-found"),
    USER_NOT_FOUND(HttpStatus.NOT_FOUND, "user-not-found"),
    ILLEGAL_PARAMETER(HttpStatus.BAD_REQUEST, "illegal-parameter"),
    ILLEGAL_DATE_TIME_FORMAT(HttpStatus.BAD_REQUEST, "illegal-date-time-format"),
    ILLEGAL_TRANSFER_AMOUNT(HttpStatus.BAD_REQUEST, "illegal-transfer-amount"),
    ILLEGAL_ACCOUNT_NUMBER(HttpStatus.BAD_REQUEST, "illegal-account-number"),
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "internal-server-error"),
    RESOURCE_NOT_FOUND(HttpStatus.NOT_FOUND, "resource-not-found"),
    METHOD_NOT_ALLOWED(HttpStatus.METHOD_NOT_ALLOWED, "method-not-allowed"),
    ;

    private final HttpStatus httpStatus;

    private final String code;

    ApiErrorCode(HttpStatus httpStatus, String code) {
        this.httpStatus = httpStatus;
        this.code = code;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    @JsonValue
    public String getCode() {
        return code;
    }

    @JsonCreator
    public static ApiErrorCode byCode(@Nonnull String code) {
        Objects.requireNonNull(code, "code");
        for (ApiErrorCode errorCode : ApiErrorCode.values()) {
            if (errorCode.getCode().equalsIgnoreCase(code)) {
                return errorCode;
            }
        }
        throw new IllegalArgumentException(String.format("Wrong error code: %s", code));
    }
}
