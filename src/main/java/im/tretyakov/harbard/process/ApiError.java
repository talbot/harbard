package im.tretyakov.harbard.process;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.http.HttpStatus;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * API error representation with code and detail message
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("API error representation")
public class ApiError {

    @ApiModelProperty(value = "Error code", required = true, example = "account-not-found")
    private final ApiErrorCode code;

    @ApiModelProperty(value = "Error details message", required = true)
    private final String message;

    @JsonCreator
    public ApiError(@JsonProperty("code") @Nonnull ApiErrorCode code, @JsonProperty("message") @Nonnull String message) {
        this.code = Objects.requireNonNull(code, "code");
        this.message = Objects.requireNonNull(message, "message");
    }

    HttpStatus getHttpStatus() {
        return code.getHttpStatus();
    }

    @JsonProperty("code")
    public String getErrorCode() {
        return code.getCode();
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ApiError{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
