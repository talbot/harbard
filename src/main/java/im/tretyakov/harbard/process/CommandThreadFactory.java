package im.tretyakov.harbard.process;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Factory for command threads creation
 */
final class CommandThreadFactory implements ThreadFactory {

    private static final Logger log = LoggerFactory.getLogger(CommandThreadFactory.class);
    private final static AtomicInteger poolNumber = new AtomicInteger(0);
    private final ThreadFactory threadFactory = Executors.defaultThreadFactory();
    private final String namePrefix;
    private final AtomicInteger threadNumber = new AtomicInteger(0);

    CommandThreadFactory(String threadPoolName) {
        namePrefix = String.format("%s-%d-", threadPoolName, poolNumber.getAndIncrement());
    }

    /**
     * Create new command thread
     *
     * @param runnable Any runnable (command) to execute
     * @return Thread to execute
     */
    @Override
    public Thread newThread(@Nonnull Runnable runnable) {
        Thread thread = threadFactory.newThread(runnable);
        thread.setName(namePrefix + threadNumber.getAndIncrement());
        thread.setUncaughtExceptionHandler((t, e) -> log.error("Detected uncaught exception in {}", t, e));
        return thread;
    }
}
