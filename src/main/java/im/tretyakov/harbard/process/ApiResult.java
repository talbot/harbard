package im.tretyakov.harbard.process;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Class-container for results of API command execution
 * @param <T> Type of result inside container
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("API result container")
public class ApiResult<T> {

    @ApiModel("Status of command execution")
    public enum Status {
        SUCCESS("success"),
        ERROR("error"),
        ;

        private final String code;

        Status(@Nonnull String code) {
            this.code = Objects.requireNonNull(code, "code");
        }

        @JsonValue
        public String getCode() {
            return code;
        }

        @JsonCreator
        public static Status byCode(@Nonnull String code) {
            Objects.requireNonNull(code, "code");
            for (Status status : Status.values()) {
                if (status.getCode().equalsIgnoreCase(code)) {
                    return status;
                }
            }
            throw new IllegalArgumentException(String.format("Wrong status code: %s", code));
        }
    }

    @ApiModelProperty(value = "Status of command execution", required = true, example = "success")
    private final Status status;

    @ApiModelProperty(value = "Error object")
    private final ApiError error;

    @ApiModelProperty(value = "Command execution result")
    private final T result;

    @JsonCreator
    private ApiResult(@JsonProperty("status") @Nonnull Status status, @JsonProperty("error") ApiError error,
                      @JsonProperty("result") T result) {
        this.status = Objects.requireNonNull(status);
        this.error = error;
        this.result = result;
    }

    @Nonnull
    public static <T> ApiResult<T> success(@Nonnull T result) {
        return new ApiResult<>(Status.SUCCESS, null, Objects.requireNonNull(result));
    }

    @Nonnull
    public static <T> ApiResult<T> error(@Nonnull ApiError error) {
        return new ApiResult<>(Status.ERROR, Objects.requireNonNull(error), null);
    }

    @JsonProperty("status")
    public Status getStatus() {
        return status;
    }

    @JsonProperty("error")
    public ApiError getError() {
        return error;
    }

    @JsonProperty("result")
    public T getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "ApiResult{" +
                "status=" + status +
                ", error=" + error +
                ", result=" + result +
                '}';
    }
}
