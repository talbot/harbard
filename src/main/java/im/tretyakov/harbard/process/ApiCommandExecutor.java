package im.tretyakov.harbard.process;

import im.tretyakov.harbard.utils.ErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static im.tretyakov.harbard.process.ApiErrorCode.INTERNAL_SERVER_ERROR;
import static im.tretyakov.harbard.utils.ErrorHandler.INTERNAL_ERROR_MESSAGE;
import static im.tretyakov.harbard.utils.ErrorHandler.getHeaders;

/**
 * Async executor for API commands
 */
public class ApiCommandExecutor {

    private static final Logger log = LoggerFactory.getLogger(ApiCommandExecutor.class);

    private final ThreadPoolExecutor executor;

    public ApiCommandExecutor(int corePoolSize, String threadPoolName) {
        this.executor = new ThreadPoolExecutor(corePoolSize, corePoolSize, 0L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(corePoolSize),
                new CommandThreadFactory(threadPoolName), new ThreadPoolExecutor.AbortPolicy());
    }

    /**
     * Executes command asynchronously
     *
     * @param command Command to execute
     * @param request Request to be handled by command
     * @param <RequestT> Request type
     * @param <ResponseT> Response type
     * @return Deferred result with response entity
     */
    @Nonnull
    public <RequestT, ResponseT> DeferredResult<ResponseEntity<ApiResult<ResponseT>>> executeCommand(
            @Nonnull ApiCommand<RequestT, ResponseT> command, RequestT request
    ) {
        log.info("Starting command execution: {}", command.getClass().getSimpleName());
        log.info("Request: {}", request);
        return async(() -> Objects.requireNonNull(command)
                .validate(request)
                .<ApiResult<ResponseT>>map(ApiResult::error)
                .orElseGet(() -> command.execute(request)));
    }

    @Nonnull
    private <ResponseT> DeferredResult<ResponseEntity<ApiResult<ResponseT>>> async(Supplier<ApiResult<ResponseT>> callable) {
        DeferredResult<ResponseEntity<ApiResult<ResponseT>>> result = new DeferredResult<>();
        executor.execute(() -> {
            try {
                ApiResult<ResponseT> response = callable.get();
                final HttpStatus status;
                if (response.getStatus() == ApiResult.Status.SUCCESS) {
                    log.info("Response: {}", response);
                    status = HttpStatus.OK;
                } else {
                    log.warn("Response: {}", response);
                    status = response.getError().getHttpStatus();
                }
                result.setResult(new ResponseEntity<>(response, status));
            } catch (Throwable t) {
                String errorId = ErrorHandler.getErrorId();
                log.error("Server error: errorId={}, error={}", errorId, t.getMessage(), t);
                result.setErrorResult(new ResponseEntity<>(
                        ApiResult.error(new ApiError(INTERNAL_SERVER_ERROR, INTERNAL_ERROR_MESSAGE)),
                        getHeaders(t, errorId), HttpStatus.INTERNAL_SERVER_ERROR));
            }
        });
        return result;
    }
}
