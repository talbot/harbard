package im.tretyakov.harbard.process;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

/**
 * Interface for API commands
 *
 * @param <RequestT> Request type
 * @param <ResponseT> Response type
 */
public interface ApiCommand<RequestT, ResponseT> {

    /**
     * Execute a command and return API result
     *
     * @param request Request to command
     * @return Execution result with response or error
     */
    @Nonnull
    ApiResult<ResponseT> execute(@Nonnull RequestT request);

    /**
     * Validates given request
     *
     * @param request Request to validate
     * @return Validation error or empty
     */
    @Nonnull
    default Optional<ApiError> validate(@Nullable RequestT request) {
        return Optional.empty();
    }
}
