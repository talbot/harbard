package im.tretyakov.harbard;

import im.tretyakov.harbard.config.HarbardConfiguration;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.boot.Banner;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.ArrayList;
import java.util.List;

@Configuration
@Import(HarbardConfiguration.class)
public class HarbardApplication {

    public static void main(String[] args) {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
        // todo: Start logging context with traceId here
        List<Class<?>> beans = new ArrayList<>();
        beans.add(HarbardApplication.class);
        SpringApplicationBuilder builder = new SpringApplicationBuilder();
        builder.bannerMode(Banner.Mode.OFF);
        builder.sources(beans.toArray(new Class<?>[0]));
        builder.run(args);
    }
}
