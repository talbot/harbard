package im.tretyakov.harbard.dao;

import im.tretyakov.harbard.domain.UserProfile;
import org.jooq.DSLContext;

import javax.annotation.Nonnull;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;

import static im.tretyakov.harbard.Sequences.USER_PROFILE_ID_SEQ;
import static im.tretyakov.harbard.Tables.USER_PROFILE;

/**
 * DAO layer for user profiles
 */
public class UserProfileDao {

    private final DSLContext dslContext;

    public UserProfileDao(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    /**
     * Creates user profile by given username and account ID
     *
     * @param userName User name
     * @param accountId Account ID
     * @return User's profile
     */
    public UserProfile createUserProfile(@Nonnull String userName, long accountId) {
        Objects.requireNonNull(userName);
        Long id = dslContext.nextval(USER_PROFILE_ID_SEQ);
        ZonedDateTime createdAt = ZonedDateTime.now();
        dslContext.insertInto(USER_PROFILE)
                .set(USER_PROFILE.ID, id)
                .set(USER_PROFILE.ACCOUNT_ID, accountId)
                .set(USER_PROFILE.USER_NAME, userName)
                .set(USER_PROFILE.CREATED_AT, createdAt.toOffsetDateTime())
                .set(USER_PROFILE.UPDATED_AT, createdAt.toOffsetDateTime())
                .execute();
        return UserProfile.builder()
                .withId(id)
                .withAccountId(accountId)
                .withUserName(userName)
                .withCreatedAt(createdAt)
                .withUpdatedAt(createdAt)
                .build();
    }

    /**
     * Finds user's profile by account ID
     *
     * @param accountId account ID
     * @return user's profile or empty
     */
    public Optional<UserProfile> findUserProfileByAccountId(long accountId) {
        return dslContext.selectFrom(USER_PROFILE)
                .where(USER_PROFILE.ACCOUNT_ID.eq(accountId))
                .fetchOptional(record -> UserProfile.builder()
                        .withId(record.getId())
                        .withUserName(record.getUserName())
                        .withAccountId(record.getAccountId())
                        .withCreatedAt(record.getCreatedAt().toZonedDateTime())
                        .withUpdatedAt(record.getUpdatedAt().toZonedDateTime())
                        .build());
    }
}
