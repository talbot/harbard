package im.tretyakov.harbard.dao;

import im.tretyakov.harbard.domain.Transfer;
import org.jooq.DSLContext;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static im.tretyakov.harbard.Sequences.TRANSFER_ID_SEQ;
import static im.tretyakov.harbard.Tables.TRANSFER;

/**
 * DAO layer for transfers
 */
public class TransferDao {

    private static final Long TECHNICAL_ACCOUNT_ID = -1L;
    private static final int DEFAULT_ROWS_LIMIT = 10;

    private final DSLContext dslContext;

    public TransferDao(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    /**
     * Refills user account balance from technical account
     *
     * @param destinationAccount Technical account ID
     * @param amount Amount of money to refill
     */
    public void refillFromTechnicalAccount(long destinationAccount, @Nonnull BigDecimal amount) {
        Objects.requireNonNull(amount, "amount");
        dslContext.insertInto(TRANSFER)
                .set(TRANSFER.SOURCE_ACCOUNT_ID, TECHNICAL_ACCOUNT_ID)
                .set(TRANSFER.DESTINATION_ACCOUNT_ID, destinationAccount)
                .set(TRANSFER.AMOUNT, amount)
                .execute();
    }

    /**
     * Creates new transfer between two accounts
     *
     * @param amount Amount to transfer
     * @param sourceAccount Source (sender) account
     * @param destinationAccount Destination (recipient) account
     * @return Result of the transfer
     */
    public Transfer createTransfer(@Nonnull BigDecimal amount, long sourceAccount, long destinationAccount) {
        Objects.requireNonNull(amount, "amount");
        ZonedDateTime transferCreationDate = ZonedDateTime.now();
        Long transferId = dslContext.nextval(TRANSFER_ID_SEQ);
        dslContext.insertInto(TRANSFER)
                .set(TRANSFER.ID, transferId)
                .set(TRANSFER.SOURCE_ACCOUNT_ID, sourceAccount)
                .set(TRANSFER.DESTINATION_ACCOUNT_ID, destinationAccount)
                .set(TRANSFER.AMOUNT, amount)
                .set(TRANSFER.CREATED_AT, transferCreationDate.toOffsetDateTime())
                .execute();
        return Transfer.builder()
                .withId(transferId)
                .withSourceAccountId(sourceAccount)
                .withDestinationAccountId(destinationAccount)
                .withAmount(amount)
                .withCreatedAt(transferCreationDate)
                .build();
    }

    /**
     * List of transfers, mapped by account ID
     *
     * @param accountId Account ID for which to lookup transfers
     * @param before Date before which we should search transfers
     * @return Map of transfers
     */
    public Map<Long, List<Transfer>> getTransfersMap(long accountId, ZonedDateTime before) {
        Map<Long, List<Transfer>> transfers = new HashMap<>();
        dslContext.selectFrom(TRANSFER)
            .where(TRANSFER.SOURCE_ACCOUNT_ID.eq(accountId)
                    .or(TRANSFER.DESTINATION_ACCOUNT_ID.eq(accountId)))
                .and(TRANSFER.SOURCE_ACCOUNT_ID.ne(TECHNICAL_ACCOUNT_ID))
                .and(TRANSFER.CREATED_AT.le(before.toOffsetDateTime()))
            .orderBy(TRANSFER.ID.desc())
            .limit(DEFAULT_ROWS_LIMIT)
            .forEach(record -> {
                Transfer transfer = Transfer.builder()
                        .withId(record.getId())
                        .withAmount(record.getAmount())
                        .withSourceAccountId(record.getSourceAccountId())
                        .withDestinationAccountId(record.getDestinationAccountId())
                        .withCreatedAt(record.getCreatedAt().toZonedDateTime())
                        .build();
                Long key = transfer.getSourceAccountId() == accountId
                        ? transfer.getDestinationAccountId()
                        : transfer.getSourceAccountId();
                if (transfers.containsKey(key)) {
                    transfers.get(key).add(transfer);
                } else {
                    List<Transfer> transferList = new ArrayList<>();
                    transferList.add(transfer);
                    transfers.put(key, transferList);
                }
            });
            return transfers;
    }
}
