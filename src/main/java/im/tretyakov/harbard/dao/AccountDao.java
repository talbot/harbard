package im.tretyakov.harbard.dao;

import im.tretyakov.harbard.domain.Account;
import im.tretyakov.harbard.domain.UserInfo;
import org.jooq.CommonTableExpression;
import org.jooq.DSLContext;
import org.jooq.Record2;
import org.jooq.impl.DSL;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static im.tretyakov.harbard.Sequences.ACCOUNT_ID_SEQ;
import static im.tretyakov.harbard.Sequences.ACCOUNT_NUMBER_SEQ;
import static im.tretyakov.harbard.Tables.ACCOUNT;
import static im.tretyakov.harbard.Tables.USER_PROFILE;

/**
 * DAO layer for accounts
 */
public class AccountDao {

    /** Start of the sequence for non-technical accounts */
    public static final Long ACCOUNT_NUMBER_SEQUENCE_START = 1002L;

    private static final CommonTableExpression<Record2<Long, BigDecimal>> BALANCE_FOR_UPDATE = DSL.name("for_update")
            .fields(ACCOUNT.ID.getName(), ACCOUNT.BALANCE.getName())
            .as(DSL.select(ACCOUNT.ID, ACCOUNT.BALANCE).from(ACCOUNT));

    private final DSLContext dslContext;

    public AccountDao(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    /**
     * Creates new account
     *
     * @param initialBalance Initial balance for account
     * @return new account
     */
    public Account createAccount(@Nonnull BigDecimal initialBalance) {
        Objects.requireNonNull(initialBalance, "initialBalance");
        Long accountId = dslContext.nextval(ACCOUNT_ID_SEQ);
        Long accountNumber = dslContext.nextval(ACCOUNT_NUMBER_SEQ);
        ZonedDateTime creationDate = ZonedDateTime.now();
        dslContext.insertInto(ACCOUNT)
                .set(ACCOUNT.ID, accountId)
                .set(ACCOUNT.ACCOUNT_NUMBER, accountNumber)
                .set(ACCOUNT.BALANCE, initialBalance)
                .set(ACCOUNT.CREATED_AT, creationDate.toOffsetDateTime())
                .set(ACCOUNT.UPDATED_AT, creationDate.toOffsetDateTime())
                .execute();
        return Account.builder()
                .withId(accountId)
                .withAccountNumber(accountNumber)
                .withCreatedAt(creationDate)
                .withUpdatedAt(creationDate)
                .withBalance(initialBalance)
                .build();
    }

    /**
     * Updates balance in lock to prevent concurrent account updating
     *
     * @param accountId Account to update
     * @param deltaAmount Sum on which account should be updated
     */
    public void updateBalance(long accountId, @Nonnull BigDecimal deltaAmount) {
        dslContext.update(ACCOUNT)
                .set(ACCOUNT.BALANCE,
                        BALANCE_FOR_UPDATE.field(ACCOUNT.BALANCE.getName(), BigDecimal.class).plus(deltaAmount))
                .from(dslContext.select(ACCOUNT.ID, ACCOUNT.BALANCE)
                        .from(ACCOUNT)
                        .where(ACCOUNT.ID.eq(accountId))
                        .forUpdate()
                        .noWait()
                        .asTable(BALANCE_FOR_UPDATE.getName()))
                .where(ACCOUNT.ID.eq(BALANCE_FOR_UPDATE.field(ACCOUNT.ID.getName(), Long.class)))
                .execute();
    }

    /**
     * Lookup account by account number
     *
     * @param accountNumber Account number
     * @return Account or empty
     */
    public Optional<Account> findAccountByNumber(long accountNumber) {
        return dslContext.selectFrom(ACCOUNT)
                .where(ACCOUNT.ACCOUNT_NUMBER.eq(accountNumber))
                .fetchOptional(record -> Account.builder()
                        .withId(record.getId())
                        .withAccountNumber(record.getAccountNumber())
                        .withBalance(record.getBalance())
                        .withCreatedAt(record.getCreatedAt().toZonedDateTime())
                        .withUpdatedAt(record.getUpdatedAt().toZonedDateTime())
                        .build());
    }

    /**
     * Map of user info, grouped by account ID
     *
     * @param accountIds Collection of account ID by which we search user info
     * @return User info mapped by account ID
     */
    public Map<Long, UserInfo> getUserInfo(@Nonnull Collection<Long> accountIds) {
        Objects.requireNonNull(accountIds, "accountIds");
        Map<Long, UserInfo> result = new HashMap<>(accountIds.size());
        dslContext.select(ACCOUNT.ID, ACCOUNT.ACCOUNT_NUMBER, USER_PROFILE.USER_NAME)
                .from(ACCOUNT)
                .join(USER_PROFILE)
                .on(USER_PROFILE.ACCOUNT_ID.eq(ACCOUNT.ID))
                .where(ACCOUNT.ID.in(accountIds))
                .forEach(row -> result.put(row.get(ACCOUNT.ID),
                        new UserInfo(row.get(USER_PROFILE.USER_NAME), row.get(ACCOUNT.ACCOUNT_NUMBER))));
        return result;
    }
}
