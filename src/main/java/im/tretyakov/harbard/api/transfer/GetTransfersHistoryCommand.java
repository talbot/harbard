package im.tretyakov.harbard.api.transfer;

import com.google.common.collect.ImmutableList;
import im.tretyakov.harbard.dao.AccountDao;
import im.tretyakov.harbard.dao.TransferDao;
import im.tretyakov.harbard.domain.Account;
import im.tretyakov.harbard.domain.Transfer;
import im.tretyakov.harbard.domain.UserInfo;
import im.tretyakov.harbard.process.ApiCommand;
import im.tretyakov.harbard.process.ApiError;
import im.tretyakov.harbard.process.ApiErrorCode;
import im.tretyakov.harbard.process.ApiResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static im.tretyakov.harbard.dao.AccountDao.ACCOUNT_NUMBER_SEQUENCE_START;
import static im.tretyakov.harbard.process.ApiErrorCode.ACCOUNT_NOT_FOUND;
import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_ACCOUNT_NUMBER;
import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_DATE_TIME_FORMAT;

public class GetTransfersHistoryCommand implements ApiCommand<GetTransfersHistoryRequest, TransfersHistoryResponse> {

    private static final Logger log = LoggerFactory.getLogger(GetTransfersHistoryCommand.class);

    private final TransferDao transferDao;

    private final AccountDao accountDao;

    public GetTransfersHistoryCommand(@Nonnull TransferDao transferDao, @Nonnull AccountDao accountDao) {
        this.transferDao = Objects.requireNonNull(transferDao, "transferDao");
        this.accountDao = Objects.requireNonNull(accountDao, "accountDao");
    }

    @Nonnull
    @Override
    public ApiResult<TransfersHistoryResponse> execute(@Nonnull GetTransfersHistoryRequest request) {
        Optional<ZonedDateTime> beforeDate = parseBeforeDate(request.getBefore());
        Optional<Account> accountOptional = accountDao.findAccountByNumber(Long.parseLong(request.getAccountNumber()));
        if (!accountOptional.isPresent()) {
            return ApiResult.error(new ApiError(ACCOUNT_NOT_FOUND, "Requested account not found"));
        }
        Account account = accountOptional.get();
        Map<Long, List<Transfer>> transfers = transferDao.getTransfersMap(account.getId(), beforeDate.orElse(ZonedDateTime.now()));
        if (transfers.isEmpty()) {
            return ApiResult.success(new TransfersHistoryResponse(ImmutableList.of()));
        }
        List<Long> accountIds = ImmutableList.<Long>builder().addAll(transfers.keySet()).add(account.getId()).build();
        Map<Long, UserInfo> userInfoMap = accountDao.getUserInfo(accountIds);
        List<TransfersHistoryItem> transferHistory = new ArrayList<>();
        transfers.forEach((accountId, transferList) -> transferList.forEach(transfer -> {
            UserInfo senderUserInfo = userInfoMap.get(transfer.getSourceAccountId());
            UserInfo recipientUserInfo = userInfoMap.get(transfer.getDestinationAccountId());
            transferHistory.add(TransfersHistoryItem.builder()
                    .withTransferId(transfer.getId())
                    .withAmount(transfer.getAmount())
                    .withDate(transfer.getCreatedAt())
                    .withSender(new TransferRecipient(senderUserInfo.getAccountNumber(), senderUserInfo.getUserName()))
                    .withRecipient(new TransferRecipient(recipientUserInfo.getAccountNumber(), recipientUserInfo.getUserName()))
                    .build());
        }));
        return ApiResult.success(new TransfersHistoryResponse(transferHistory));
    }

    @Nonnull
    @Override
    public Optional<ApiError> validate(@Nullable GetTransfersHistoryRequest request) {
        if (request == null) {
            return Optional.of(new ApiError(ApiErrorCode.ILLEGAL_PARAMETER,
                    "Transfer history request could not be null"));
        }
        if (!StringUtils.isNumeric(request.getAccountNumber())) {
            return Optional.of(new ApiError(ApiErrorCode.ILLEGAL_PARAMETER, "Account should be a number"));
        }
        if (Long.parseLong(request.getAccountNumber()) < ACCOUNT_NUMBER_SEQUENCE_START) {
            return Optional.of(new ApiError(ILLEGAL_ACCOUNT_NUMBER, "Illegal account number"));
        }
        if (StringUtils.isNotBlank(request.getBefore()) && !parseBeforeDate(request.getBefore()).isPresent()) {
            return Optional.of(new ApiError(ILLEGAL_DATE_TIME_FORMAT,
                    "Before date parameter should be in ISO8601 Zoned date time"));
        }
        return Optional.empty();
    }

    private static Optional<ZonedDateTime> parseBeforeDate(@Nullable String before) {
        if (StringUtils.isBlank(before)) {
            return Optional.empty();
        }
        try {
            return Optional.of(ZonedDateTime.parse(before, DateTimeFormatter.ISO_ZONED_DATE_TIME));
        } catch (DateTimeParseException e) {
            log.warn("Cannot parse date-time string: dateTime={}, index={}, error={}", before, e.getErrorIndex(), e.getMessage());
        }
        return Optional.empty();
    }
}
