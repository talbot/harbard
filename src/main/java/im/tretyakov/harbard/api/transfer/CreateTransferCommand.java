package im.tretyakov.harbard.api.transfer;

import im.tretyakov.harbard.dao.AccountDao;
import im.tretyakov.harbard.dao.TransferDao;
import im.tretyakov.harbard.dao.UserProfileDao;
import im.tretyakov.harbard.domain.Account;
import im.tretyakov.harbard.domain.Transfer;
import im.tretyakov.harbard.domain.UserProfile;
import im.tretyakov.harbard.process.ApiCommand;
import im.tretyakov.harbard.process.ApiError;
import im.tretyakov.harbard.process.ApiResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

import static im.tretyakov.harbard.dao.AccountDao.ACCOUNT_NUMBER_SEQUENCE_START;
import static im.tretyakov.harbard.process.ApiErrorCode.ACCOUNT_NOT_FOUND;
import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_ACCOUNT_NUMBER;
import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_PARAMETER;
import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_TRANSFER_AMOUNT;
import static im.tretyakov.harbard.process.ApiErrorCode.USER_NOT_FOUND;

public class CreateTransferCommand implements ApiCommand<CreateTransferRequest, CreateTransferResponse> {

    private final TransferDao transferDao;

    private final AccountDao accountDao;

    private final UserProfileDao userProfileDao;

    private final TransactionTemplate txTemplate;

    public CreateTransferCommand(@Nonnull TransferDao transferDao, @Nonnull AccountDao accountDao,
                                 @Nonnull UserProfileDao userProfileDao, @Nonnull TransactionTemplate txTemplate) {
        this.transferDao = Objects.requireNonNull(transferDao, "transferDao");
        this.accountDao = Objects.requireNonNull(accountDao, "accountDao");
        this.userProfileDao = Objects.requireNonNull(userProfileDao, "userProfileDao");
        this.txTemplate = Objects.requireNonNull(txTemplate, "txTemplate");
    }

    @Nonnull
    @Override
    public ApiResult<CreateTransferResponse> execute(@Nonnull CreateTransferRequest request) {
        Optional<Account> sourceAccountOptional = accountDao.findAccountByNumber(request.getSenderAccountNumber());
        if (!sourceAccountOptional.isPresent()) {
            return ApiResult.error(new ApiError(ACCOUNT_NOT_FOUND, "Requested account not found"));
        }
        Account sourceAccount = sourceAccountOptional.get();
        if (sourceAccount.getBalance().subtract(request.getAmount()).compareTo(BigDecimal.ZERO) < 0) {
            return ApiResult.error(new ApiError(ILLEGAL_TRANSFER_AMOUNT, "Transfer amount is more than source account balance"));
        }
        Optional<UserProfile> sourceUserProfile = userProfileDao.findUserProfileByAccountId(sourceAccount.getId());
        if (!sourceUserProfile.isPresent()) {
            return ApiResult.error(new ApiError(USER_NOT_FOUND, "Requested user not found"));
        }
        Optional<Account> destinationAccountOptional = accountDao.findAccountByNumber(request.getRecipientAccountNumber());
        if (!destinationAccountOptional.isPresent()) {
            return ApiResult.error(new ApiError(ACCOUNT_NOT_FOUND, "Requested account not found"));
        }
        Account destinationAccount = destinationAccountOptional.get();
        Optional<UserProfile> destinationUserProfile = userProfileDao.findUserProfileByAccountId(destinationAccount.getId());
        if (!destinationUserProfile.isPresent()) {
            return ApiResult.error(new ApiError(USER_NOT_FOUND, "Requested user not found"));
        }
        Transfer transfer = txTemplate.execute(txCallback -> {
            accountDao.updateBalance(sourceAccount.getId(), request.getAmount().negate());
            accountDao.updateBalance(destinationAccount.getId(), request.getAmount());
            return transferDao.createTransfer(request.getAmount(), sourceAccount.getId(),
                    destinationAccount.getId());
        });
        return ApiResult.success(CreateTransferResponse.builder()
                .withTransferId(transfer.getId())
                .withAmount(transfer.getAmount())
                .withDate(transfer.getCreatedAt())
                .withSender(new TransferRecipient(sourceAccount.getAccountNumber(), sourceUserProfile.get().getUserName()))
                .withRecipient(new TransferRecipient(destinationAccount.getAccountNumber(), destinationUserProfile.get().getUserName()))
                .build());
    }

    @Nonnull
    @Override
    public Optional<ApiError> validate(@Nullable CreateTransferRequest request) {
        if (request == null) {
            return Optional.of(new ApiError(ILLEGAL_PARAMETER, "Request could not be null"));
        }
        if (request.getSenderAccountNumber() == null || request.getSenderAccountNumber() < ACCOUNT_NUMBER_SEQUENCE_START) {
            return Optional.of(new ApiError(ILLEGAL_ACCOUNT_NUMBER, "Illegal sender account number"));
        }
        if (request.getRecipientAccountNumber() == null || request.getRecipientAccountNumber() < ACCOUNT_NUMBER_SEQUENCE_START) {
            return Optional.of(new ApiError(ILLEGAL_ACCOUNT_NUMBER, "Illegal recipient account number"));
        }
        if (request.getSenderAccountNumber().equals(request.getRecipientAccountNumber())) {
            return Optional.of(new ApiError(ILLEGAL_ACCOUNT_NUMBER, "Sender and recipient accounts must be different"));
        }
        if (request.getAmount() == null || request.getAmount().compareTo(BigDecimal.ONE) < 0) {
            return Optional.of(new ApiError(ILLEGAL_TRANSFER_AMOUNT, "Illegal transfer amount"));
        }
        return Optional.empty();
    }
}
