package im.tretyakov.harbard.api.transfer;

import im.tretyakov.harbard.process.ApiCommandExecutor;
import im.tretyakov.harbard.process.ApiResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import javax.annotation.Nonnull;
import java.util.Objects;

@RestController
@RequestMapping(
        path = "/api/v1/transfers",
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class TransferController {

    private final ApiCommandExecutor executor;

    private final CreateTransferCommand createTransferCommand;

    private final GetTransfersHistoryCommand getTransfersHistoryCommand;

    public TransferController(@Nonnull ApiCommandExecutor executor, @Nonnull CreateTransferCommand createTransferCommand,
                              @Nonnull GetTransfersHistoryCommand getTransfersHistoryCommand) {
        this.executor = Objects.requireNonNull(executor, "apiCommandExecutor");
        this.createTransferCommand = Objects.requireNonNull(createTransferCommand, "createTransferCommand");
        this.getTransfersHistoryCommand = Objects.requireNonNull(getTransfersHistoryCommand, "getTransfersHistoryCommand");
    }

    @ApiOperation(value = "New money transfer creation", response = CreateTransferResponse.class)
    @RequestMapping(
            method = RequestMethod.POST,
            value = "",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @Nonnull
    public DeferredResult<ResponseEntity<ApiResult<CreateTransferResponse>>> createTransfer(
            @RequestBody CreateTransferRequest request) {
        return executor.executeCommand(createTransferCommand, request);
    }

    @ApiOperation(value = "List of money transfers", response = TransfersHistoryResponse.class)
    @RequestMapping(method = RequestMethod.GET, value = "{accountNumber}")
    @Nonnull
    public DeferredResult<ResponseEntity<ApiResult<TransfersHistoryResponse>>> getTransferHistory(
            @PathVariable("accountNumber") String accountNumber,
            @RequestParam(name = "before", required = false) String dateBefore
    ) {
        return executor.executeCommand(getTransfersHistoryCommand, new GetTransfersHistoryRequest(accountNumber, dateBefore));
    }
}
