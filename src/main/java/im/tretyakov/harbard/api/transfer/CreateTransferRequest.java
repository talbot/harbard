package im.tretyakov.harbard.api.transfer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("Request for transfer between accounts")
public class CreateTransferRequest {

    @ApiModelProperty(value = "Sender (source) account number for transfer", required = true, example = "1004")
    private final Long senderAccountNumber;

    @ApiModelProperty(value = "Recipient (destination) account number for transfer", required = true, example = "1016")
    private final Long recipientAccountNumber;

    @ApiModelProperty(value = "Amount to transfer", required = true, example = "20.00")
    private final BigDecimal amount;

    @JsonCreator
    private CreateTransferRequest(@JsonProperty("sender") Long senderAccountNumber,
                                  @JsonProperty("recipient") Long recipientAccountNumber,
                                  @JsonProperty("amount") BigDecimal amount) {
        this.senderAccountNumber = senderAccountNumber;
        this.recipientAccountNumber = recipientAccountNumber;
        this.amount = amount;
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonProperty("sender")
    Long getSenderAccountNumber() {
        return senderAccountNumber;
    }

    @JsonProperty("recipient")
    Long getRecipientAccountNumber() {
        return recipientAccountNumber;
    }

    @JsonProperty("amount")
    BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "CreateTransferRequest{" +
                "sender_account_number=" + senderAccountNumber +
                ", recipient_account_number=" + recipientAccountNumber +
                ", amount=" + amount +
                '}';
    }

    public static final class Builder {
        private Long senderAccountNumber;
        private Long recipientAccountNumber;
        private BigDecimal amount;

        private Builder() {
        }

        @Nonnull
        public Builder withSenderAccountNumber(Long senderAccountNumber) {
            this.senderAccountNumber = Objects.requireNonNull(senderAccountNumber, "sender_account_number");
            return this;
        }

        @Nonnull
        public Builder withRecipientAccountNumber(Long recipientAccountNumber) {
            this.recipientAccountNumber = Objects.requireNonNull(recipientAccountNumber, "recipient_account_number");
            return this;
        }

        @Nonnull
        public Builder withAmount(BigDecimal amount) {
            this.amount = Objects.requireNonNull(amount, "amount");
            return this;
        }

        @Nonnull
        public CreateTransferRequest build() {
            return new CreateTransferRequest(senderAccountNumber, recipientAccountNumber, amount);
        }
    }
}
