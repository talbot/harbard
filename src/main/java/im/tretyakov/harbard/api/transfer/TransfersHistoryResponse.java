package im.tretyakov.harbard.api.transfer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;

@ApiModel("Transfer history")
public class TransfersHistoryResponse {

    @ApiModelProperty(value = "List of transfer history items", required = true)
    private final List<TransfersHistoryItem> history;

    @JsonCreator
    TransfersHistoryResponse(@JsonProperty("history") @Nonnull List<TransfersHistoryItem> history) {
        this.history = Objects.requireNonNull(history, "history");
    }

    @JsonProperty("history")
    @Nonnull
    public List<TransfersHistoryItem> getHistory() {
        return history;
    }

    @Override
    public String toString() {
        return "TransfersHistoryResponse{" +
                "history=" + history +
                '}';
    }
}
