package im.tretyakov.harbard.api.transfer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("Response for transfer creation")
public class CreateTransferResponse {

    @ApiModelProperty(value = "Unique transfer identifier", required = true, example = "14")
    private final Long transferId;

    @ApiModelProperty(value = "Transfer sender", required = true)
    private final TransferRecipient sender;

    @ApiModelProperty(value = "Transfer recipient", required = true)
    private final TransferRecipient recipient;

    @ApiModelProperty(value = "Transfer amount", required = true, example = "20.00")
    private final BigDecimal amount;

    @ApiModelProperty(value = "Date of the transfer", required = true, example = "2017-09-21T12:34:56.789+03:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private final ZonedDateTime date;

    @JsonCreator
    private CreateTransferResponse(@JsonProperty("transfer_id") @Nonnull Long transferId,
                                   @JsonProperty("sender") @Nonnull TransferRecipient sender,
                                   @JsonProperty("recipient") @Nonnull TransferRecipient recipient,
                                   @JsonProperty("amount") @Nonnull BigDecimal amount,
                                   @JsonProperty("date") @Nonnull ZonedDateTime date) {
        this.transferId = Objects.requireNonNull(transferId, "transfer_id");
        this.sender = Objects.requireNonNull(sender, "sender");
        this.recipient = Objects.requireNonNull(recipient, "recipient");
        this.amount = Objects.requireNonNull(amount, "amount");
        this.date = Objects.requireNonNull(date, "date");
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonProperty("transfer_id")
    @Nonnull
    public Long getTransferId() {
        return transferId;
    }

    @JsonProperty("sender")
    @Nonnull
    public TransferRecipient getSender() {
        return sender;
    }

    @JsonProperty("recipient")
    @Nonnull
    public TransferRecipient getRecipient() {
        return recipient;
    }

    @JsonProperty("amount")
    @Nonnull
    public BigDecimal getAmount() {
        return amount;
    }

    @JsonProperty("date")
    @Nonnull
    public ZonedDateTime getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "CreateTransferResponse{" +
                "transfer_id=" + transferId +
                ", sender=" + sender +
                ", recipient=" + recipient +
                ", amount=" + amount +
                ", date=" + date +
                '}';
    }

    public static final class Builder {
        private Long transferId;
        private TransferRecipient sender;
        private TransferRecipient recipient;
        private BigDecimal amount;
        private ZonedDateTime date;

        private Builder() {
        }

        @Nonnull
        public Builder withTransferId(@Nonnull Long transferId) {
            this.transferId = transferId;
            return this;
        }

        @Nonnull
        public Builder withSender(@Nonnull TransferRecipient sender) {
            this.sender = sender;
            return this;
        }

        @Nonnull
        public Builder withRecipient(@Nonnull TransferRecipient recipient) {
            this.recipient = recipient;
            return this;
        }

        @Nonnull
        public Builder withAmount(@Nonnull BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        @Nonnull
        public Builder withDate(@Nonnull ZonedDateTime date) {
            this.date = date;
            return this;
        }

        @Nonnull
        public CreateTransferResponse build() {
            return new CreateTransferResponse(transferId, sender, recipient, amount, date);
        }
    }
}
