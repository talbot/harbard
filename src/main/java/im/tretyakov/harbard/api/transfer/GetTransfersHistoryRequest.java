package im.tretyakov.harbard.api.transfer;

public class GetTransfersHistoryRequest {

    private final String accountNumber;

    private final String before;

    public GetTransfersHistoryRequest(String accountNumber, String before) {
        this.accountNumber = accountNumber;
        this.before = before;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getBefore() {
        return before;
    }

    @Override
    public String toString() {
        return "GetTransfersHistoryRequest{" +
                "accountNumber='" + accountNumber + '\'' +
                ", before=" + before +
                '}';
    }
}
