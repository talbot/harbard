package im.tretyakov.harbard.api.transfer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransfersHistoryItem {

    private final Long transferId;

    @ApiModelProperty(value = "Date of the transfer", required = true, example = "2017-09-21T12:34:56.789+03:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private final ZonedDateTime date;

    private final BigDecimal amount;

    private final TransferRecipient sender;

    private final TransferRecipient recipient;

    @JsonCreator
    private TransfersHistoryItem(@JsonProperty("transfer_id") @Nonnull Long transferId, @JsonProperty("date") @Nonnull ZonedDateTime date,
                                 @JsonProperty("amount") @Nonnull BigDecimal amount,
                                 @JsonProperty("sender") @Nonnull TransferRecipient sender,
                                 @JsonProperty("recipient") @Nonnull TransferRecipient recipient) {
        this.transferId = Objects.requireNonNull(transferId, "transfer_id");
        this.date = Objects.requireNonNull(date, "date");
        this.amount = Objects.requireNonNull(amount, "amount");
        this.sender = Objects.requireNonNull(sender, "sender");
        this.recipient = Objects.requireNonNull(recipient, "recipient");
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonProperty("transfer_id")
    @Nonnull
    public Long getTransferId() {
        return transferId;
    }

    @JsonProperty("date")
    @Nonnull
    public ZonedDateTime getDate() {
        return date;
    }

    @JsonProperty("amount")
    @Nonnull
    public BigDecimal getAmount() {
        return amount;
    }

    @JsonProperty("sender")
    @Nonnull
    public TransferRecipient getSender() {
        return sender;
    }

    @JsonProperty("recipient")
    @Nonnull
    public TransferRecipient getRecipient() {
        return recipient;
    }

    @Override
    public String toString() {
        return "TransfersHistoryItem{" +
                "date=" + date +
                ", amount=" + amount +
                ", sender=" + sender +
                ", recipient=" + recipient +
                '}';
    }

    public static final class Builder {
        private Long transferId;
        private ZonedDateTime date;
        private BigDecimal amount;
        private TransferRecipient sender;
        private TransferRecipient recipient;

        private Builder() {
        }

        @Nonnull
        public Builder withTransferId(@Nonnull Long transferId) {
            this.transferId = transferId;
            return this;
        }

        @Nonnull
        public Builder withDate(@Nonnull ZonedDateTime date) {
            this.date = date;
            return this;
        }

        @Nonnull
        public Builder withAmount(@Nonnull BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        @Nonnull
        public Builder withSender(@Nonnull TransferRecipient sender) {
            this.sender = sender;
            return this;
        }

        @Nonnull
        public Builder withRecipient(@Nonnull TransferRecipient recipient) {
            this.recipient = recipient;
            return this;
        }

        @Nonnull
        public TransfersHistoryItem build() {
            return new TransfersHistoryItem(transferId, date, amount, sender, recipient);
        }
    }
}
