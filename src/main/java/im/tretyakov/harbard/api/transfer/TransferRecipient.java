package im.tretyakov.harbard.api.transfer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.annotation.Nonnull;
import java.util.Objects;

@ApiModel("Transfer recipient: source or destination of the transfer")
public class TransferRecipient {

    @ApiModelProperty(value = "Recipient account number", required = true, example = "1004")
    private final Long accountNumber;

    @ApiModelProperty(value = "Recipient user name", required = true, example = "John Doe")
    private final String userName;

    @JsonCreator
    public TransferRecipient(@JsonProperty("account_number") @Nonnull Long accountNumber,
                             @JsonProperty("user_name") @Nonnull String userName) {
        this.accountNumber = Objects.requireNonNull(accountNumber, "account_number");
        this.userName = Objects.requireNonNull(userName, "user_name");
    }

    @JsonProperty("account_number")
    @Nonnull
    public Long getAccountNumber() {
        return accountNumber;
    }

    @JsonProperty("user_name")
    @Nonnull
    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return "TransferRecipient{" +
                "account_number=" + accountNumber +
                ", user_name='" + userName + '\'' +
                '}';
    }
}
