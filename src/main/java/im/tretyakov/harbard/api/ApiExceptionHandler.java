package im.tretyakov.harbard.api;

import im.tretyakov.harbard.utils.ErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import java.util.Arrays;

import static im.tretyakov.harbard.process.ApiErrorCode.INTERNAL_SERVER_ERROR;
import static im.tretyakov.harbard.process.ApiErrorCode.METHOD_NOT_ALLOWED;
import static im.tretyakov.harbard.process.ApiErrorCode.RESOURCE_NOT_FOUND;
import static im.tretyakov.harbard.utils.ErrorHandler.INTERNAL_ERROR_MESSAGE;
import static im.tretyakov.harbard.utils.ErrorHandler.handleError;

@RestControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
                                                             HttpStatus status, WebRequest request) {
        String errorId = ErrorHandler.getErrorId();
        switch (status) {
            case NOT_FOUND:
                return handleError(RESOURCE_NOT_FOUND, ex.getMessage(), ex, errorId);
            case METHOD_NOT_ALLOWED:
                return handleError(METHOD_NOT_ALLOWED, ex.getMessage(), ex, errorId);
            default:
                log.error("API global exception handler caught exception: errorId={}, body={}, status={}, headers={}, request={}",
                        errorId, body, status, headers, request, ex);
                return handleError(INTERNAL_SERVER_ERROR, INTERNAL_ERROR_MESSAGE, ex, errorId);
        }
    }

    @ExceptionHandler(Throwable.class)
    ResponseEntity<Object> handleThrowables(@Nullable Throwable throwable) {
        String errorId = ErrorHandler.getErrorId();
        logError(throwable, errorId);
        return handleError(INTERNAL_SERVER_ERROR, INTERNAL_ERROR_MESSAGE, throwable, errorId);
    }

    private static void logError(@Nullable Throwable error, @Nonnull String errorId) {
        if (error == null) {
            log.error("API global exception handler caught empty exception: errorId={}, threadStackTrace={}",
                    errorId, Arrays.toString(Thread.currentThread().getStackTrace()));
        } else {
            log.error("API global exception handler caught exception: errorId={}, error={}",
                    errorId, error.getMessage(), error);
        }
    }
}
