package im.tretyakov.harbard.api.account;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.Objects;

@ApiModel("Response for account creation")
public class AccountInfoResponse {

    @ApiModelProperty(value = "Bank account number", required = true, example = "41000112")
    private final Long accountNumber;

    @ApiModelProperty(value = "User name", required = true, example = "John Doe")
    private final String userName;

    @ApiModelProperty(value = "Current balance", required = true, example = "20.00")
    private final BigDecimal balance;

    @JsonCreator
    private AccountInfoResponse(@JsonProperty("account_number") @Nonnull Long accountNumber,
                                @JsonProperty("user_name") @Nonnull String userName,
                                @JsonProperty("balance") @Nonnull  BigDecimal balance) {
        this.accountNumber = Objects.requireNonNull(accountNumber, "account_number");
        this.userName = Objects.requireNonNull(userName, "user_name");
        this.balance = Objects.requireNonNull(balance, "balance");
    }

    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("account_number")
    public Long getAccountNumber() {
        return accountNumber;
    }

    @Nonnull
    @JsonProperty("user_name")
    public String getUserName() {
        return userName;
    }

    @Nonnull
    @JsonProperty("balance")
    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "AccountInfoResponse{" +
                "accountNumber=" + accountNumber +
                ", userName='" + userName + '\'' +
                ", balance=" + balance +
                '}';
    }

    public static final class Builder {
        private Long accountNumber;
        private String userName;
        private BigDecimal balance;

        private Builder() {
        }

        @Nonnull
        public Builder withAccountNumber(@Nonnull Long accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        @Nonnull
        public Builder withUserName(@Nonnull String userName) {
            this.userName = userName;
            return this;
        }

        @Nonnull
        public Builder withBalance(@Nonnull BigDecimal balance) {
            this.balance = balance;
            return this;
        }

        @Nonnull
        public AccountInfoResponse build() {
            return new AccountInfoResponse(accountNumber, userName, balance);
        }
    }
}
