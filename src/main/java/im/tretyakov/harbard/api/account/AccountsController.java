package im.tretyakov.harbard.api.account;

import im.tretyakov.harbard.process.ApiCommandExecutor;
import im.tretyakov.harbard.process.ApiResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import javax.annotation.Nonnull;
import java.util.Objects;

@RestController
@RequestMapping(
        path = "/api/v1/accounts",
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class AccountsController {

    private final ApiCommandExecutor executor;

    private final CreateAccountCommand createAccountCommand;

    private final GetAccountInfoCommand getAccountInfoCommand;

    public AccountsController(@Nonnull ApiCommandExecutor executor, @Nonnull CreateAccountCommand createAccountCommand,
                              @Nonnull GetAccountInfoCommand getAccountInfoCommand) {
        this.executor = Objects.requireNonNull(executor, "apiCommandExecutor");
        this.createAccountCommand = Objects.requireNonNull(createAccountCommand, "createAccountCommand");
        this.getAccountInfoCommand = Objects.requireNonNull(getAccountInfoCommand, "getAccountInfoCommand");
    }

    @ApiOperation(value = "User account creation", response = AccountInfoResponse.class)
    @RequestMapping(
            method = RequestMethod.POST,
            value = "",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @Nonnull
    public DeferredResult<ResponseEntity<ApiResult<AccountInfoResponse>>> createAccount(
            @RequestBody CreateAccountRequest request) {
        return executor.executeCommand(createAccountCommand, request);
    }

    @ApiOperation(value = "Retrieves account info", response = AccountInfoResponse.class)
    @RequestMapping(
            method = RequestMethod.GET,
            value = "{accountNumber}"
    )
    @Nonnull
    public DeferredResult<ResponseEntity<ApiResult<AccountInfoResponse>>> getAccountInfo(
            @PathVariable("accountNumber") String accountNumber) {
        return executor.executeCommand(getAccountInfoCommand, accountNumber);
    }
}
