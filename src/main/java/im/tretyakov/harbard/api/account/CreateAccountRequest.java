package im.tretyakov.harbard.api.account;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.Objects;

@ApiModel("Request for new account creation")
public class CreateAccountRequest {

    @ApiModelProperty(value = "Expected name for the user", required = true, example = "John Doe")
    private final String userName;

    @ApiModelProperty(value = "Expected initial balance", required = true, example = "20.00")
    private final BigDecimal initialBalance;

    @JsonCreator
    private CreateAccountRequest(@JsonProperty("user_name") String userName,
                                 @JsonProperty("initial_balance") BigDecimal initialBalance) {
        this.userName = userName;
        this.initialBalance = initialBalance;
    }

    public static Builder bulder() {
        return new Builder();
    }

    @JsonProperty("user_name")
    public String getUserName() {
        return userName;
    }

    @JsonProperty("initial_balance")
    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    @Override
    public String toString() {
        return "CreateAccountRequest{" +
                "userName='" + userName + '\'' +
                ", initialBalance=" + initialBalance +
                '}';
    }

    public static final class Builder {
        private String userName;
        private BigDecimal initialBalance;

        private Builder() {
        }

        @Nonnull
        public Builder withUserName(@Nonnull String userName) {
            this.userName = Objects.requireNonNull(userName, "user_name");
            return this;
        }

        @Nonnull
        public Builder withInitialBalance(@Nonnull BigDecimal initialBalance) {
            this.initialBalance = Objects.requireNonNull(initialBalance, "initial_balance");
            return this;
        }

        @Nonnull
        public CreateAccountRequest build() {
            return new CreateAccountRequest(userName, initialBalance);
        }
    }
}
