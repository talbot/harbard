package im.tretyakov.harbard.api.account;

import im.tretyakov.harbard.dao.AccountDao;
import im.tretyakov.harbard.dao.UserProfileDao;
import im.tretyakov.harbard.domain.Account;
import im.tretyakov.harbard.domain.UserProfile;
import im.tretyakov.harbard.process.ApiCommand;
import im.tretyakov.harbard.process.ApiError;
import im.tretyakov.harbard.process.ApiErrorCode;
import im.tretyakov.harbard.process.ApiResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;
import java.util.Optional;

import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_ACCOUNT_NUMBER;

public class GetAccountInfoCommand implements ApiCommand<String, AccountInfoResponse> {

    private static final Logger log = LoggerFactory.getLogger(GetAccountInfoCommand.class);
    private final AccountDao accountDao;
    private final UserProfileDao userProfileDao;

    public GetAccountInfoCommand(@Nonnull AccountDao accountDao, @Nonnull UserProfileDao userProfileDao) {
        this.accountDao = Objects.requireNonNull(accountDao, "accountDao");
        this.userProfileDao = Objects.requireNonNull(userProfileDao, "userProfileDao");
    }

    @Nonnull
    @Override
    public ApiResult<AccountInfoResponse> execute(@Nullable String accountId) {
        Objects.requireNonNull(accountId);
        Optional<Account> accountOptional = accountDao.findAccountByNumber(Long.parseLong(accountId));
        if (!accountOptional.isPresent()) {
            log.warn("Account not found: accountId={}", accountId);
            return ApiResult.error(new ApiError(ApiErrorCode.ACCOUNT_NOT_FOUND,
                    String.format("Account %s not found", accountId)));
        }
        Account account = accountOptional.get();
        Optional<UserProfile> profileOptional = userProfileDao.findUserProfileByAccountId(account.getId());
        if (!profileOptional.isPresent()) {
            log.warn("User not found: accountId={}", accountId);
            return ApiResult.error(new ApiError(ApiErrorCode.USER_NOT_FOUND,
                    String.format("User of account %s not found", accountId)));
        }
        return ApiResult.success(AccountInfoResponse.builder()
                .withAccountNumber(account.getAccountNumber())
                .withUserName(profileOptional.get().getUserName())
                .withBalance(account.getBalance())
                .build());
    }

    @SuppressWarnings("ConstantConditions") // We're checking that request is not null in StringUtils#isNumeric
    @Nonnull
    @Override
    public Optional<ApiError> validate(@Nullable String request) {
        if (!StringUtils.isNumeric(request)) {
            return Optional.of(new ApiError(ApiErrorCode.ILLEGAL_PARAMETER, "Parameter should be a number"));
        }
        long accountNumber = Long.parseLong(request);
        if (accountNumber < AccountDao.ACCOUNT_NUMBER_SEQUENCE_START) {
            return Optional.of(new ApiError(ILLEGAL_ACCOUNT_NUMBER, "Illegal account number"));
        }
        return Optional.empty();
    }
}
