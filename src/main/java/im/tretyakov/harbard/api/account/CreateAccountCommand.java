package im.tretyakov.harbard.api.account;

import im.tretyakov.harbard.dao.AccountDao;
import im.tretyakov.harbard.dao.TransferDao;
import im.tretyakov.harbard.dao.UserProfileDao;
import im.tretyakov.harbard.domain.Account;
import im.tretyakov.harbard.domain.UserProfile;
import im.tretyakov.harbard.process.ApiCommand;
import im.tretyakov.harbard.process.ApiError;
import im.tretyakov.harbard.process.ApiResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_PARAMETER;

public class CreateAccountCommand implements ApiCommand<CreateAccountRequest, AccountInfoResponse> {

    private final AccountDao accountDao;

    private final TransferDao transferDao;

    private final UserProfileDao userProfileDao;

    private final TransactionTemplate txTemplate;

    public CreateAccountCommand(@Nonnull AccountDao accountDao, @Nonnull TransferDao transferDao,
                                @Nonnull UserProfileDao userProfileDao, @Nonnull TransactionTemplate txTemplate) {
        this.accountDao = Objects.requireNonNull(accountDao, "accountDao");
        this.transferDao = Objects.requireNonNull(transferDao, "transferDao");
        this.userProfileDao = Objects.requireNonNull(userProfileDao, "userProfileDao");
        this.txTemplate = Objects.requireNonNull(txTemplate, "transactionTemplate");
    }

    @Nonnull
    @Override
    public ApiResult<AccountInfoResponse> execute(@Nonnull CreateAccountRequest request) {
        AccountInfoResponse response = txTemplate.execute(txCallback -> {
            Account account = accountDao.createAccount(request.getInitialBalance());
            transferDao.refillFromTechnicalAccount(account.getId(), request.getInitialBalance());
            UserProfile userProfile = userProfileDao.createUserProfile(request.getUserName(), account.getId());

            return AccountInfoResponse.builder()
                    .withUserName(userProfile.getUserName())
                    .withAccountNumber(account.getAccountNumber())
                    .withBalance(account.getBalance())
                    .build();
        });
        return ApiResult.success(response);
    }

    @Nonnull
    @Override
    public Optional<ApiError> validate(@Nullable CreateAccountRequest request) {
        if (request == null) {
            return Optional.of(new ApiError(ILLEGAL_PARAMETER, "Request could not be null"));
        }
        if (StringUtils.isBlank(request.getUserName())) {
            return Optional.of(new ApiError(ILLEGAL_PARAMETER, "User name is empty"));
        }
        if (request.getInitialBalance() == null) {
            return Optional.of(new ApiError(ILLEGAL_PARAMETER, "Initial balance expected"));
        }
        if (request.getInitialBalance().compareTo(BigDecimal.ZERO) < 0) {
            return Optional.of(new ApiError(ILLEGAL_PARAMETER, "Balance can not be negative"));
        }
        return Optional.empty();
    }
}
