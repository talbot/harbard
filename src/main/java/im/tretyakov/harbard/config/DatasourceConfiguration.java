package im.tretyakov.harbard.config;

import im.tretyakov.harbard.config.properties.DatasourceProperties;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListener;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

@Configuration
@PropertySources({
        @PropertySource(value = "classpath:config/datasource.properties"),
        @PropertySource(value = "classpath:override.properties"),
})
public class DatasourceConfiguration {

    @Bean
    public DatasourceProperties datasourceProperties() {
        return new DatasourceProperties();
    }

    @Bean(initMethod = "init")
    PostgresDataSource dataSource(DatasourceProperties datasourceProperties) {
        return PostgresDataSource.fromProperties(datasourceProperties);
    }

    @Bean
    DataSourceTransactionManager transactionManager(PostgresDataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    TransactionTemplate transactionTemplate(DataSourceTransactionManager transactionManager) {
        return new TransactionTemplate(transactionManager);
    }

    @Bean
    TransactionTemplate requiresNewTransactionTemplate(DataSourceTransactionManager transactionManager) {
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        return transactionTemplate;
    }

    @Bean
    JdbcTemplate masterJdbcTemplate(PostgresDataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    TransactionAwareDataSourceProxy dataSourceProxy(PostgresDataSource dataSource) {
        return new TransactionAwareDataSourceProxy(dataSource);
    }

    @Bean
    DSLContext dslContext(TransactionAwareDataSourceProxy dataSourceProxy) {
        DataSourceConnectionProvider provider = new DataSourceConnectionProvider(dataSourceProxy);
        DefaultConfiguration configuration = new DefaultConfiguration();
        configuration.setSQLDialect(SQLDialect.POSTGRES_9_5);
        configuration.setConnectionProvider(provider);
        configuration.setExecuteListenerProvider(new DefaultExecuteListenerProvider(new DefaultExecuteListener()));
        return new DefaultDSLContext(configuration);
    }
}
