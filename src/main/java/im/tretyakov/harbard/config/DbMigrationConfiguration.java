package im.tretyakov.harbard.config;

import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class DbMigrationConfiguration {

    @Bean
    Flyway flyway(PostgresDataSource dataSource) {
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setOutOfOrder(true);
        flyway.setBaselineOnMigrate(true);
        flyway.setLocations("sql");
        flyway.migrate();
        return flyway;
    }
}
