package im.tretyakov.harbard.config;

import im.tretyakov.harbard.dao.AccountDao;
import im.tretyakov.harbard.dao.TransferDao;
import im.tretyakov.harbard.dao.UserProfileDao;
import org.jooq.DSLContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class DatabaseConfiguration {

    @Bean
    AccountDao accountDao(DSLContext dslContext) {
        return new AccountDao(dslContext);
    }

    @Bean
    TransferDao transferDao(DSLContext dslContext) {
        return new TransferDao(dslContext);
    }

    @Bean
    UserProfileDao userProfileDao(DSLContext dslContext) {
        return new UserProfileDao(dslContext);
    }
}
