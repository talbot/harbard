package im.tretyakov.harbard.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import im.tretyakov.harbard.utils.ApiObjectMapper;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

public class ConvertersConfiguration extends WebMvcConfigurerAdapter {

    private static final ObjectMapper MAPPER = ApiObjectMapper.createObjectMapper();

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new MappingJackson2HttpMessageConverter(MAPPER));
    }
}
