package im.tretyakov.harbard.config;

import im.tretyakov.harbard.config.properties.ServerProperties;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.JreMemoryLeakPreventionListener;
import org.apache.catalina.core.ThreadLocalLeakPreventionListener;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.startup.VersionLoggerListener;
import org.apache.coyote.http11.Http11Nio2Protocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.embedded.EmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

@Configuration
public class ServerConfiguration {

    private static final Logger log = LoggerFactory.getLogger(ServerConfiguration.class);

    @Bean
    public TomcatServletFactory embeddedServletContainerFactory(ServerProperties serverProperties) {
        TomcatServletFactory servletFactory = new TomcatServletFactory("harbard");
        servletFactory.addContextLifecycleListeners(new VersionLoggerListener());
        servletFactory.addContextLifecycleListeners(new ThreadLocalLeakPreventionListener());
        servletFactory.addContextLifecycleListeners(new JreMemoryLeakPreventionListener());
        servletFactory.setPrimaryConnector(createConnector(serverProperties));
        return servletFactory;
    }

    public static class TomcatServletFactory extends TomcatEmbeddedServletContainerFactory {
        private final String applicationName;
        private Connector primaryConnector;

        public TomcatServletFactory(String applicationName) {
            this.applicationName = applicationName;
        }

        void setPrimaryConnector(Connector connector) {
            this.primaryConnector = Objects.requireNonNull(connector);
        }

        @Override
        public EmbeddedServletContainer getEmbeddedServletContainer(ServletContextInitializer... initializers) {
            Objects.requireNonNull(primaryConnector);
            Tomcat tomcat = new Tomcat();
            tomcat.setBaseDir(createTempDir(applicationName + "_tomcat").getAbsolutePath());
            tomcat.getHost().setAutoDeploy(false);
            for (Connector connector : tomcat.getService().findConnectors()) {
                tomcat.getService().removeConnector(connector);
            }
            tomcat.getService().addConnector(primaryConnector);
            tomcat.setConnector(primaryConnector);
            tomcat.setPort(primaryConnector.getPort());
            tomcat.getEngine().setBackgroundProcessorDelay(-1);
            prepareContext(tomcat.getHost(), initializers);
            return new TomcatEmbeddedServletContainer(tomcat, true);
        }
    }

    private static Connector createConnector(ServerProperties serverProperties) {
        log.info("Configuring connector");
        Connector connector = new Connector(Http11Nio2Protocol.class.getCanonicalName());
        connector.setPort(serverProperties.getPort());
        connector.setAttribute("enabled", true);
        connector.setAttribute("maxThreads", serverProperties.getMaxThreads());
        connector.setAttribute("keepAliveTimeout", serverProperties.getKeepAliveTimeout());
        connector.setAttribute("connectionAliveTimeout", serverProperties.getConnectionAliveTimeout());
        connector.setAttribute("maxKeepAliveRequests", serverProperties.getMaxKeepAliveRequests());
        return connector;
    }
}
