package im.tretyakov.harbard.config;

import im.tretyakov.harbard.config.properties.DatasourceProperties;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

class PostgresDataSource extends BasicDataSource {

    private static final Logger log = LoggerFactory.getLogger(PostgresDataSource.class);

    public void init() throws SQLException {
        log.info("Initializing connection pool");
        super.createDataSource();
        log.info("Connection pool initialized: active_count={}, idle_count={}", getNumActive(), getNumIdle());
    }

    static PostgresDataSource fromProperties(DatasourceProperties properties) {
        final PostgresDataSource dataSource = new PostgresDataSource();
        dataSource.setDriverClassName(properties.getDriverClassName());
        dataSource.setUsername(properties.getUsername());
        dataSource.setPassword(properties.getPassword());
        dataSource.setValidationQuery(properties.getValidationQuery());
        dataSource.setValidationQueryTimeout(properties.getValidationQueryTimeout());
        dataSource.setMaxTotal(properties.getMaxTotal());
        dataSource.setMaxIdle(properties.getMaxIdle());
        dataSource.setMinIdle(properties.getMinIdle());
        dataSource.setMaxWaitMillis(properties.getMaxWaitTimeMillis());
        dataSource.setRemoveAbandonedTimeout(properties.getRemoveAbandonedTimeout());
        dataSource.setRemoveAbandonedOnBorrow(properties.getRemoveAbandonedOnBorrow());
        dataSource.setRemoveAbandonedOnMaintenance(properties.getRemoveAbandonedOnMaintenance());
        dataSource.setDefaultAutoCommit(properties.getDefaultAutoCommit());
        dataSource.setDefaultReadOnly(properties.getDefaultReadOnly());
        dataSource.setTestWhileIdle(properties.getTestWhileIdle());
        dataSource.setTestOnBorrow(properties.getTestOnBorrow());
        dataSource.setTestOnReturn(properties.getTestOnReturn());
        dataSource.setUrl(properties.getUrl());
        return dataSource;
    }
}
