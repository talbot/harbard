package im.tretyakov.harbard.config;

import im.tretyakov.harbard.api.ApiExceptionHandler;
import im.tretyakov.harbard.api.account.AccountsController;
import im.tretyakov.harbard.api.account.CreateAccountCommand;
import im.tretyakov.harbard.api.account.GetAccountInfoCommand;
import im.tretyakov.harbard.api.transfer.CreateTransferCommand;
import im.tretyakov.harbard.api.transfer.GetTransfersHistoryCommand;
import im.tretyakov.harbard.api.transfer.TransferController;
import im.tretyakov.harbard.config.properties.DatasourceProperties;
import im.tretyakov.harbard.config.properties.ServerProperties;
import im.tretyakov.harbard.dao.AccountDao;
import im.tretyakov.harbard.dao.TransferDao;
import im.tretyakov.harbard.dao.UserProfileDao;
import im.tretyakov.harbard.process.ApiCommandExecutor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.support.TransactionTemplate;

@Configuration
@Import({
        ApplicationConfiguration.class,
        ServerConfiguration.class,
        DatasourceConfiguration.class,
        DbMigrationConfiguration.class,
        DatabaseConfiguration.class,
        ConvertersConfiguration.class,
        SwaggerConfiguration.class,

        ServerProperties.class,
        DatasourceProperties.class
})
public class HarbardConfiguration {

    @Bean
    ApiExceptionHandler apiExceptionHandler() {
        return new ApiExceptionHandler();
    }

    @Bean
    ApiCommandExecutor commandExecutor(ServerProperties serverProperties) {
        return new ApiCommandExecutor(serverProperties.getThreadPoolSize(), serverProperties.getThreadPoolName());
    }

    @Bean
    CreateAccountCommand createAccountCommand(AccountDao accountDao, TransferDao transferDao,
                                              UserProfileDao userProfileDao,
                                              TransactionTemplate requiresNewTransactionTemplate) {
        return new CreateAccountCommand(accountDao, transferDao, userProfileDao, requiresNewTransactionTemplate);
    }

    @Bean
    GetAccountInfoCommand getAccountInfoCommand(AccountDao accountDao, UserProfileDao userProfileDao) {
        return new GetAccountInfoCommand(accountDao, userProfileDao);
    }

    @Bean
    AccountsController accountsController(ApiCommandExecutor executor, CreateAccountCommand createAccountCommand,
                                          GetAccountInfoCommand getAccountInfoCommand) {
        return new AccountsController(executor, createAccountCommand, getAccountInfoCommand);
    }

    @Bean
    CreateTransferCommand createTransferCommand(TransactionTemplate transactionTemplate, AccountDao accountDao,
                                                TransferDao transferDao, UserProfileDao userProfileDao) {
        return new CreateTransferCommand(transferDao, accountDao, userProfileDao, transactionTemplate);
    }

    @Bean
    GetTransfersHistoryCommand getTransfersHistoryCommand(AccountDao accountDao, TransferDao transferDao) {
        return new GetTransfersHistoryCommand(transferDao, accountDao);
    }

    @Bean
    TransferController transferController(ApiCommandExecutor executor, CreateTransferCommand createTransferCommand,
                                          GetTransfersHistoryCommand getTransfersHistoryCommand) {
        return new TransferController(executor, createTransferCommand, getTransfersHistoryCommand);
    }
}
