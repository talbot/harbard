package im.tretyakov.harbard;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import im.tretyakov.harbard.process.ApiResult;
import im.tretyakov.harbard.utils.ApiObjectMapper;
import org.eclipse.jetty.client.api.ContentResponse;

import java.io.IOException;
import java.math.BigDecimal;

public final class TestUtils {

    private static final ObjectMapper MAPPER = ApiObjectMapper.createObjectMapper();

    public static final String TEST_USER = "John A. Doe";
    public static final BigDecimal TEST_AMOUNT = BigDecimal.valueOf(123.45D);

    public static <T> ApiResult<T> convertResponse(ContentResponse response, Class<T> responseClass) throws IOException {
        JavaType type = MAPPER.getTypeFactory().constructParametricType(ApiResult.class, responseClass);
        return MAPPER.readValue(response.getContent(), type);
    }
}
