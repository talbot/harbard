package im.tretyakov.harbard;

import com.fasterxml.jackson.databind.ObjectMapper;
import im.tretyakov.harbard.utils.ApiObjectMapper;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.util.StringContentProvider;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import ru.yandex.qatools.embed.postgresql.Command;
import ru.yandex.qatools.embed.postgresql.PostgresProcess;
import ru.yandex.qatools.embed.postgresql.PostgresStarter;
import ru.yandex.qatools.embed.postgresql.config.AbstractPostgresConfig;
import ru.yandex.qatools.embed.postgresql.config.PostgresConfig;
import ru.yandex.qatools.embed.postgresql.config.RuntimeConfigBuilder;
import ru.yandex.qatools.embed.postgresql.distribution.Version;

import java.io.IOException;

import static org.testng.Assert.fail;

public abstract class AbstractEndToEndTest extends AbstractTestNGSpringContextTests {

    private static final String BASE_URL = "http://localhost:8091/api/v1/";

    private static final ObjectMapper MAPPER = ApiObjectMapper.createObjectMapper();

    private final HttpClient httpClient = new HttpClient();

    private PostgresProcess postgresProcess = null;

    @BeforeSuite
    public void beforeSuite() throws Exception {
        try {
            postgresProcess = PostgresStarter.getInstance(new RuntimeConfigBuilder()
                    .defaults(Command.Postgres)
                    .build())
                    .prepare(
                            new PostgresConfig(Version.Main.V9_5,
                                    new AbstractPostgresConfig.Net("127.0.0.1", 25432),
                                    new AbstractPostgresConfig.Storage("pg_test"),
                                    new AbstractPostgresConfig.Timeout(6000L),
                                    new AbstractPostgresConfig.Credentials("pg_test", "pg_test"))
                    )
                    .start();
            httpClient.start();
        } catch (IOException e) {
            httpClient.stop();
            if (postgresProcess != null) {
                postgresProcess.stop();
            }
            fail(e.getMessage(), e);
        }
    }

    @AfterSuite
    public void afterSuite() throws Exception {
        if (postgresProcess != null) {
            postgresProcess.stop();
        }
        httpClient.stop();
    }

    protected ContentResponse post(String resource, Object requestContent) throws Exception {
        Request request = httpClient.POST(BASE_URL + resource);
        String requestBody = MAPPER.writeValueAsString(requestContent);
        request.content(new StringContentProvider(requestBody), MediaType.APPLICATION_JSON_UTF8_VALUE);
        return request.send();
    }

    protected ContentResponse get(String resource) throws Exception {
        return httpClient.GET(BASE_URL + resource);
    }
}
