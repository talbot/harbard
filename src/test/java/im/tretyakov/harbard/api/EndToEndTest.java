package im.tretyakov.harbard.api;

import im.tretyakov.harbard.AbstractEndToEndTest;
import im.tretyakov.harbard.api.account.AccountInfoResponse;
import im.tretyakov.harbard.api.account.CreateAccountRequest;
import im.tretyakov.harbard.api.transfer.CreateTransferRequest;
import im.tretyakov.harbard.api.transfer.CreateTransferResponse;
import im.tretyakov.harbard.api.transfer.TransfersHistoryResponse;
import im.tretyakov.harbard.config.HarbardConfiguration;
import im.tretyakov.harbard.process.ApiResult;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.http.HttpStatus;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static im.tretyakov.harbard.TestUtils.TEST_AMOUNT;
import static im.tretyakov.harbard.TestUtils.TEST_USER;
import static im.tretyakov.harbard.TestUtils.convertResponse;
import static im.tretyakov.harbard.process.ApiErrorCode.RESOURCE_NOT_FOUND;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

@SpringBootTest(webEnvironment = DEFINED_PORT, classes = EndToEndTest.class)
@Import(HarbardConfiguration.class)
public class EndToEndTest extends AbstractEndToEndTest {

    private Long accountNumber = 0L;
    private Long recipientAccountNumber = 0L;
    private Long transferId = 0L;

    @Test(description = "Create account: happy path")
    public void testCreateAccount() throws Exception {
        // given
        CreateAccountRequest request = CreateAccountRequest.bulder()
                .withInitialBalance(TEST_AMOUNT)
                .withUserName(TEST_USER)
                .build();

        // when
        ContentResponse response = post("accounts", request);

        // then
        assertEquals(response.getStatus(), HttpStatus.OK_200);
        ApiResult<AccountInfoResponse> result = convertResponse(response, AccountInfoResponse.class);
        assertEquals(result.getStatus(), ApiResult.Status.SUCCESS);
        assertNull(result.getError());
        assertNotNull(result.getResult());
        AccountInfoResponse accountInfo = result.getResult();
        accountNumber = accountInfo.getAccountNumber();
        assertEquals(accountInfo.getBalance().compareTo(TEST_AMOUNT), 0);
        assertEquals(accountInfo.getUserName(), TEST_USER);
    }

    @Test(description = "Get Account info test (happy path)", dependsOnMethods = "testCreateAccount")
    public void testGetAccountInfo() throws Exception {
        ContentResponse response = get("accounts/" + accountNumber);
        assertEquals(response.getStatus(), HttpStatus.OK_200);
        ApiResult<AccountInfoResponse> result = convertResponse(response, AccountInfoResponse.class);
        assertEquals(result.getStatus(), ApiResult.Status.SUCCESS);
        assertNull(result.getError());
        assertNotNull(result.getResult());
        AccountInfoResponse accountInfo = result.getResult();
        assertEquals(accountInfo.getBalance().compareTo(TEST_AMOUNT), 0);
        assertEquals(accountInfo.getUserName(), TEST_USER);
    }

    @Test(description = "Test of new transfer creation")
    public void testCreateNewTransfer() throws Exception {
        // given
        CreateAccountRequest createRecipientAccountRequest = CreateAccountRequest.bulder()
                .withUserName(TEST_USER.replace('o', 'i'))
                .withInitialBalance(TEST_AMOUNT)
                .build();
        ContentResponse recipientAccount = post("accounts", createRecipientAccountRequest);
        assertEquals(recipientAccount.getStatus(), HttpStatus.OK_200);
        ApiResult<AccountInfoResponse> recipientResult = convertResponse(recipientAccount, AccountInfoResponse.class);
        recipientAccountNumber = recipientResult.getResult().getAccountNumber();

        CreateTransferRequest transferRequest = CreateTransferRequest.builder()
                .withSenderAccountNumber(accountNumber)
                .withRecipientAccountNumber(recipientAccountNumber)
                .withAmount(BigDecimal.ONE)
                .build();

        // when
        ContentResponse response = post("transfers", transferRequest);

        // then
        assertEquals(response.getStatus(), HttpStatus.OK_200);
        ApiResult<CreateTransferResponse> result = convertResponse(response, CreateTransferResponse.class);
        assertEquals(result.getStatus(), ApiResult.Status.SUCCESS);
        assertEquals(result.getResult().getAmount().compareTo(BigDecimal.ONE), 0);
        assertEquals(result.getResult().getSender().getAccountNumber(), accountNumber);
        assertEquals(result.getResult().getRecipient().getAccountNumber(), recipientAccountNumber);
        assertEquals(result.getResult().getSender().getUserName(), TEST_USER);
        transferId = result.getResult().getTransferId();
    }

    @Test(description = "Get transfer history", dependsOnMethods = "testCreateNewTransfer")
    public void testGetTransferHistory() throws Exception {
        // given
        CreateTransferRequest request = CreateTransferRequest.builder()
                .withSenderAccountNumber(accountNumber)
                .withRecipientAccountNumber(recipientAccountNumber)
                .withAmount(BigDecimal.ONE)
                .build();
        ContentResponse createTransferResponse = post("transfers", request);
        assertEquals(createTransferResponse.getStatus(), HttpStatus.OK_200);
        ApiResult<CreateTransferResponse> result = convertResponse(createTransferResponse, CreateTransferResponse.class);

        // when
        ContentResponse historyResponse = get("transfers/" + accountNumber);

        // then
        assertEquals(historyResponse.getStatus(), HttpStatus.OK_200);
        ApiResult<TransfersHistoryResponse> historyResult = convertResponse(historyResponse, TransfersHistoryResponse.class);
        assertEquals(historyResult.getStatus(), ApiResult.Status.SUCCESS);
        assertEquals(historyResult.getResult().getHistory().size(), 2);
        assertTrue(historyResult.getResult().getHistory().stream()
                .anyMatch(history -> history.getTransferId().equals(transferId)));
        assertTrue(historyResult.getResult().getHistory().stream()
                .anyMatch(history -> history.getTransferId().equals(result.getResult().getTransferId())));
    }

    @Test(description = "Get 404 on wrong resource")
    public void testErrorOnWrongResource() throws Exception {
        ContentResponse errorResponse = get("transfer/" + accountNumber);
        assertEquals(errorResponse.getStatus(), HttpStatus.NOT_FOUND_404);
        ApiResult<?> result = convertResponse(errorResponse, Object.class);
        assertEquals(result.getStatus(), ApiResult.Status.ERROR);
        assertEquals(result.getError().getErrorCode(), RESOURCE_NOT_FOUND.getCode());
    }
}
