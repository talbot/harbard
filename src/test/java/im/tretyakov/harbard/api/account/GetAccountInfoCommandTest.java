package im.tretyakov.harbard.api.account;

import im.tretyakov.harbard.dao.AccountDao;
import im.tretyakov.harbard.dao.UserProfileDao;
import im.tretyakov.harbard.process.ApiError;
import im.tretyakov.harbard.process.ApiErrorCode;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Optional;

import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_ACCOUNT_NUMBER;
import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_PARAMETER;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class GetAccountInfoCommandTest {

    private final GetAccountInfoCommand command =
            new GetAccountInfoCommand(mock(AccountDao.class), mock(UserProfileDao.class));

    @Test(dataProvider = "accountInfoErrors")
    public void validateTest(String request, ApiErrorCode code, String expectedMessage) {
        Optional<ApiError> error = command.validate(request);
        assertTrue(error.isPresent());
        assertEquals(error.get().getMessage(), expectedMessage);
        assertEquals(error.get().getErrorCode(), code.getCode());
    }

    @DataProvider
    private static Object[][] accountInfoErrors() {
        return new Object[][] {
                {null, ILLEGAL_PARAMETER, "Parameter should be a number"},
                {" ", ILLEGAL_PARAMETER, "Parameter should be a number"},
                {"abcDOO", ILLEGAL_PARAMETER, "Parameter should be a number"},
                {"1", ILLEGAL_ACCOUNT_NUMBER, "Illegal account number"},
        };
    }
}