package im.tretyakov.harbard.api.account;

import im.tretyakov.harbard.dao.AccountDao;
import im.tretyakov.harbard.dao.TransferDao;
import im.tretyakov.harbard.dao.UserProfileDao;
import im.tretyakov.harbard.process.ApiError;
import im.tretyakov.harbard.process.ApiErrorCode;
import org.springframework.transaction.support.TransactionTemplate;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static im.tretyakov.harbard.TestUtils.TEST_AMOUNT;
import static im.tretyakov.harbard.TestUtils.TEST_USER;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.*;

/**
 * Test of command for account creation
 */
public class CreateAccountCommandTest {

    @Test(dataProvider = "requestErrorDataProvider")
    public void testRequestValidation(CreateAccountRequest request, String expectedMessage) {
        CreateAccountCommand command = new CreateAccountCommand(mock(AccountDao.class),
                mock(TransferDao.class), mock(UserProfileDao.class), mock(TransactionTemplate.class));
        Optional<ApiError> error = command.validate(request);
        assertTrue(error.isPresent());
        assertEquals(error.get().getErrorCode(), ApiErrorCode.ILLEGAL_PARAMETER.getCode());
        assertEquals(error.get().getMessage(), expectedMessage);
    }

    @DataProvider
    private static Object[][] requestErrorDataProvider() {
        return new Object[][] {
                {null, "Request could not be null"},
                {CreateAccountRequest.bulder().withUserName(TEST_USER).build(), "Initial balance expected"},
                {CreateAccountRequest.bulder().withInitialBalance(TEST_AMOUNT).build(), "User name is empty"},
                {CreateAccountRequest.bulder().withUserName(TEST_USER).withInitialBalance(BigDecimal.valueOf(-1.00D)).build(),
                        "Balance can not be negative"}
        };
    }
}