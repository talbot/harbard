package im.tretyakov.harbard.api.transfer;

import im.tretyakov.harbard.dao.AccountDao;
import im.tretyakov.harbard.dao.TransferDao;
import im.tretyakov.harbard.dao.UserProfileDao;
import im.tretyakov.harbard.process.ApiError;
import im.tretyakov.harbard.process.ApiErrorCode;
import org.springframework.transaction.support.TransactionTemplate;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static im.tretyakov.harbard.TestUtils.TEST_AMOUNT;
import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_ACCOUNT_NUMBER;
import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_PARAMETER;
import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_TRANSFER_AMOUNT;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.*;

public class CreateTransferCommandTest {

    private final CreateTransferCommand command = new CreateTransferCommand(mock(TransferDao.class),
            mock(AccountDao.class), mock(UserProfileDao.class), mock(TransactionTemplate.class));

    @Test(dataProvider = "errorsDataProvider")
    public void testValidate(CreateTransferRequest request, ApiErrorCode code, String expectedMessage) throws Exception {
        Optional<ApiError> error = command.validate(request);
        assertTrue(error.isPresent());
        assertEquals(error.get().getErrorCode(), code.getCode());
        assertEquals(error.get().getMessage(), expectedMessage);
    }

    @DataProvider
    private static Object[][] errorsDataProvider() {
        return new Object[][] {
                {null, ILLEGAL_PARAMETER, "Request could not be null"},
                {CreateTransferRequest.builder()
                        .withAmount(TEST_AMOUNT)
                        .withRecipientAccountNumber(1102L)
                        .build(), ILLEGAL_ACCOUNT_NUMBER, "Illegal sender account number"},
                {CreateTransferRequest.builder()
                        .withAmount(TEST_AMOUNT)
                        .withSenderAccountNumber(102L)
                        .withRecipientAccountNumber(1102L)
                        .build(), ILLEGAL_ACCOUNT_NUMBER, "Illegal sender account number"},
                {CreateTransferRequest.builder()
                        .withAmount(TEST_AMOUNT)
                        .withSenderAccountNumber(1102L)
                        .build(), ILLEGAL_ACCOUNT_NUMBER, "Illegal recipient account number"},
                {CreateTransferRequest.builder()
                        .withAmount(TEST_AMOUNT)
                        .withRecipientAccountNumber(102L)
                        .withSenderAccountNumber(1102L)
                        .build(), ILLEGAL_ACCOUNT_NUMBER, "Illegal recipient account number"},
                {CreateTransferRequest.builder()
                        .withAmount(TEST_AMOUNT)
                        .withRecipientAccountNumber(1102L)
                        .withSenderAccountNumber(1102L)
                        .build(), ILLEGAL_ACCOUNT_NUMBER, "Sender and recipient accounts must be different"},
                {CreateTransferRequest.builder()
                        .withAmount(BigDecimal.ZERO)
                        .withRecipientAccountNumber(1102L)
                        .withSenderAccountNumber(1104L)
                        .build(), ILLEGAL_TRANSFER_AMOUNT, "Illegal transfer amount"},
                {CreateTransferRequest.builder()
                        .withAmount(BigDecimal.valueOf(-1.00D))
                        .withRecipientAccountNumber(1102L)
                        .withSenderAccountNumber(1104L)
                        .build(), ILLEGAL_TRANSFER_AMOUNT, "Illegal transfer amount"},

        };
    }
}