package im.tretyakov.harbard.api.transfer;

import im.tretyakov.harbard.dao.AccountDao;
import im.tretyakov.harbard.dao.TransferDao;
import im.tretyakov.harbard.process.ApiError;
import im.tretyakov.harbard.process.ApiErrorCode;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Optional;

import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_ACCOUNT_NUMBER;
import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_DATE_TIME_FORMAT;
import static im.tretyakov.harbard.process.ApiErrorCode.ILLEGAL_PARAMETER;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.*;

public class GetTransfersHistoryCommandTest {

    private final GetTransfersHistoryCommand command = new GetTransfersHistoryCommand(mock(TransferDao.class),
            mock(AccountDao.class));

    @Test(dataProvider = "errorsDataProvider")
    public void testValidate(GetTransfersHistoryRequest request, ApiErrorCode code, String message) throws Exception {
        Optional<ApiError> error = command.validate(request);
        assertTrue(error.isPresent());
        assertEquals(error.get().getErrorCode(), code.getCode());
        assertEquals(error.get().getMessage(), message);
    }

    @DataProvider
    private static Object[][] errorsDataProvider() {
        return new Object[][] {
                {null, ILLEGAL_PARAMETER, "Transfer history request could not be null"},
                {new GetTransfersHistoryRequest(null, null), ILLEGAL_PARAMETER, "Account should be a number"},
                {new GetTransfersHistoryRequest(" ", null), ILLEGAL_PARAMETER, "Account should be a number"},
                {new GetTransfersHistoryRequest("ir-34", null), ILLEGAL_PARAMETER, "Account should be a number"},
                {new GetTransfersHistoryRequest("123", null), ILLEGAL_ACCOUNT_NUMBER, "Illegal account number"},
                {new GetTransfersHistoryRequest("1202", "12-12-12"), ILLEGAL_DATE_TIME_FORMAT,
                        "Before date parameter should be in ISO8601 Zoned date time"},
        };
    }
}