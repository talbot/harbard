# Application description

**This application do four basic things:**
- Creates user account with name and initial balance: `POST /api/v1/accounts`
- Do transfers between accounts: `POST /api/v1/transfers`
- Get account information like user name, account number and current balance: `GET /api/v1/accounts/%account_number%`
- Get transfers history, paged by 10 elements: `GET /api/v1/transfers/%account_number%?before=%transfers_before_date%`

By default, application runs on `localhost` on `8091` port and is accessible via HTTP protocol. 
Application requires Postgres database to start, which default configuration settings 
are in `/src/main/resources/config/datasource.properties` file.

## Used technologies

This application written with Java 8 and requires JDK 8 to build. Dependency and build management is through Maven.
To build application one should generate sources and package app:

``mvn clean embedded-postgresql:start flyway:migrate jooq-codegen:generate embedded-postgresql:stop add-sources compile package``

To run application from sources all you need to run next command:

``mvn spring-boot:run``

As HTTP server application uses embedded Tomcat server. 
For persistence layer application uses Postgres database. 
Application uses Yandex Embedded Postgres database for code generation and testing purposes.
Application provides migration through Flyway. First, Flyway migrates schema into embedded Postgres for code generation purposes, 
then when application starts, Flyway do migration into application database.
When doing integration tests, embedded Postgres runs too and Flyway migrates database schema into embedded test database.
First code generation or test run may be slow because of embedded database downloading.
Tests including unit tests and end-to-end tests of application business processes.
Additionally, application, when started, provides on-line documentation through Swagger, 
which is available at `http://127.0.0.1:8091/swagger-ui.html`

## Security concerns

Application does not uses any authentication, authorization and identification technologies, 
which are necessary for applications of that kind. The ways to advance the application in this field 
could include implementation of different authentication, 
authorization and identification technologies (like basic auth, OAuth, JWT, SSL certificates etc.), 
which definitely should be done in other µ-services and connected to this service.  

## Performance concerns

This application implements mostly basic functions, has no long-running transactions or calls to other services, 
so it's hard to fail in this field. But when it comes to production, 
we could use different technologies like database partitioning and sharding, aggressive caching, 
short-circuiting calls to unresponsive services etc.      